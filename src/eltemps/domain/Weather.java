package eltemps.domain;

public class Weather {

    private String ciudad;
    private Double temp;
    private Double feelsLike;
    private Double min;
    private Double max;
    private Double pressure;
    private Integer humidity;
    private String description;
    private String icon;
    private Double speed;
    private Integer deg;
    private Integer clouds;
    private Double rain;
    private Double snow;
    private String date;

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Double getTemp() { return temp; }

    public void setTemp(Double temp) { this.temp = temp; }

    public Double getFeelsLike() { return feelsLike; }

    public void setFeelsLike(Double feelsLike) { this.feelsLike = feelsLike; }

    public Double getMin() {
        return min;
    }

    public void setMin(Double min) {
        this.min = min;
    }

    public Double getMax() {
        return max;
    }

    public void setMax(Double max) {
        this.max = max;
    }

    public Double getPressure() {
        return pressure;
    }

    public void setPressure(Double pressure) {
        this.pressure = pressure;
    }

    public Integer getHumidity() {
        return humidity;
    }

    public void setHumidity(Integer humidity) {
        this.humidity = humidity;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Double getSpeed() {
        return speed;
    }

    public void setSpeed(Double speed) {
        this.speed = speed;
    }

    public Integer getDeg() {
        return deg;
    }

    public void setDeg(Integer deg) {
        this.deg = deg;
    }

    public Integer getClouds() {
        return clouds;
    }

    public void setClouds(Integer clouds) {
        this.clouds = clouds;
    }

    public Double getRain() {
        return rain;
    }

    public void setRain(Double rain) {
        this.rain = rain;
    }

    public Double getSnow() {
        return snow;
    }

    public void setSnow(Double snow) {
        this.snow = snow;
    }

    @Override
    public String toString() {
        return "Weather{" +
                "temp=" + temp +
                ", feelsLike=" + feelsLike +
                ", min=" + min +
                ", max=" + max +
                ", pressure=" + pressure +
                ", humidity=" + humidity +
                ", description='" + description + '\'' +
                ", icon='" + icon + '\'' +
                ", speed=" + speed +
                ", deg=" + deg +
                ", clouds=" + clouds +
                ", rain=" + rain +
                ", snow=" + snow +
                ", date='" + date + '\'' +
                '}';
    }
}
