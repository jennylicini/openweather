
package eltemps.domain;

import java.util.ArrayList;
import java.util.List;

public class Forecast {

    private String cod;
    private Double message;
    private String city;
    private Integer cnt;
    private  List<Weather> list = new ArrayList<>();


    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public Double getMessage() {
        return message;
    }

    public void setMessage(Double message) {
        this.message = message;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Integer getCnt() {
        return cnt;
    }

    public void setCnt(Integer cnt) {
        this.cnt = cnt;
    }

    public java.util.List<Weather> getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }


    @Override
    public String toString() {
        return "Forecast{" +
                "cod='" + cod + '\'' +
                ", message=" + message +
                ", city=" + city +
                ", cnt=" + cnt +
                ", list=" + list +
                '}';
    }
}
