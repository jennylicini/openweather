package eltemps;

import eltemps.domain.Weather;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class lvlcForecast extends ListCell<Weather> {

    @FXML
    private Label lblciudad;

    @FXML
    private Label lblfecha;

    @FXML
    private ImageView imgicon;

    @FXML
    private Label lblgrado;

    @FXML
    private Label lbldescripcion;

    @FXML
    private Label lblmax;

    @FXML
    private Label lblmin;

    private FXMLLoader mLLoader;

    private List<Weather> allFiveWeather = new ArrayList<>();

    @FXML
    private AnchorPane AnchorPane;


   /* public void setListWeather(List<Weather> weather, String city){
        allFiveWeather.addAll( weather);
        this.ciudad=city;
        System.out.println(ciudad);
    }*/


    @Override
    protected void updateItem(Weather weather, boolean empty) {
        super.updateItem(weather, empty);

        if (empty || weather == null) {
            setText(null);
            setGraphic(null);
        } else {
            //New List cell
            if (mLLoader == null) {
                mLLoader = new FXMLLoader(getClass().getResource("ui/lcForecast.fxml"));
                mLLoader.setController(this);
                try {
                    mLLoader.load();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            //Defenir los labels con la informacion

            lblciudad.setText(weather.getCiudad());

            String dateStr = weather.getDate();

            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            DateFormat formatter1 = new SimpleDateFormat("dd/MM/yyyy");
            try {
                lblfecha.setText(formatter1.format(formatter.parse(dateStr)));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            lblgrado.setText(weather.getTemp()+"°C");
            lbldescripcion.setText(weather.getDescription());
            lblmax.setText("max: "+weather.getMax()+"°C");
            lblmin.setText("min: "+weather.getMin()+"°C");

            String img = "http://openweathermap.org/img/wn/" + weather.getIcon() + "@2x.png";
            showImage(img);


            setGraphic(AnchorPane);

        }
    }

    private void showImage(String img) {
        try {
            javafx.scene.image.Image image = new Image(img);
            imgicon.setImage(image);
            imgicon.setCache(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
