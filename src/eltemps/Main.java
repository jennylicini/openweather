package eltemps;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class Main extends Application {

    /**
     *
     * @web http://java-buddy.blogspot.com/
     */

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("ui/lvWeather.fxml"));
        Image image = new Image("https://openweathermap.org/themes/openweathermap/assets/img/mobile_app/android_icon.png");
        primaryStage.getIcons().add(image);
        primaryStage.setTitle("Open Weather - Tiempo Actual");
        primaryStage.setScene(new Scene(root, 365, 453));
        primaryStage.setResizable(false);
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
