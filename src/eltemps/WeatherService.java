package eltemps;

import eltemps.domain.Forecast;
import eltemps.domain.Weather;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.IOException;

public class WeatherService {

    String forecastJSON;
    

    public Weather getCurrentWeather(String city)
    {
        String crida = "https://api.openweathermap.org/data/2.5/weather?q=" + city + "&units=metric&lang=es&appid=d26e513aca410c9d3e09a576db0671ba";

        forecastJSON = null;

        try {
            forecastJSON = owmCall (crida);
        } catch (IOException e) {
            //e.printStackTrace();
            return null;
        }

        JSONObject joWeather = (JSONObject) new JSONTokener(forecastJSON).nextValue();

        return recogerDatos(joWeather, true, city);
    }

    public Forecast getFiveDay (String city)
    {
        int cont = 0; //0 8 16 24 32

        //https://api.openweathermap.org/data/2.5/forecast?q=barcelona&lang=es&appid=d26e513aca410c9d3e09a576db0671ba
        String crida = "https://api.openweathermap.org/data/2.5/forecast?q=" + city + "&units=metric&lang=es&appid=d26e513aca410c9d3e09a576db0671ba";
        Forecast forecast = new Forecast(); // New city

        forecast.setCity(city);
        forecastJSON = null;

        try {
            forecastJSON = owmCall (crida);
        } catch (IOException e) {
            return null;
        }
        JSONObject joForecastWeather = (JSONObject) new JSONTokener(forecastJSON).nextValue();

        forecast.setCod(joForecastWeather.getString("cod"));
        forecast.setMessage(joForecastWeather.getDouble("message"));
        forecast.setCnt(joForecastWeather.getInt("cnt"));

        //5 days == 0 8 16 24 32
        while (cont <= 32) {
            JSONObject joList = joForecastWeather.getJSONArray("list").getJSONObject(cont);
            //Add to list of weather
            forecast.getList().add(recogerDatos(joList, false, city));
            cont += 8;
        }

        return forecast;
    }

    //true = getCurrentWeather false=getFiveDay
    private Weather recogerDatos(JSONObject joWeather, boolean api, String city)
    {
        JSONObject joMain = joWeather.getJSONObject("main");
        JSONObject joDesc = joWeather.getJSONArray("weather").getJSONObject(0);
        JSONObject joWind = joWeather.getJSONObject("wind");
        JSONObject joClouds= joWeather.getJSONObject("clouds");

        Weather current = new Weather();

        if (!api){
            current.setDate(joWeather.getString("dt_txt"));
            current.setCiudad(city);
        }

        current.setTemp(joMain.getDouble("temp"));
        current.setFeelsLike(joMain.getDouble("feels_like"));
        current.setMin(joMain.getDouble("temp_min"));
        current.setMax(joMain.getDouble("temp_max"));
        current.setPressure(joMain.getDouble("pressure"));
        current.setHumidity(joMain.getInt("humidity"));
        current.setDescription(joDesc.getString("description"));
        current.setIcon(joDesc.getString("icon"));
        current.setSpeed(joWind.getDouble("speed"));
        current.setClouds(joClouds.getInt("all"));


        if (joWeather.has("rain")){
            JSONObject joRain = joWeather.getJSONObject("rain");
            if (api)current.setRain(joRain.getDouble("1h"));
        }
        if (joWeather.has("snow")){
            JSONObject joSnow = joWeather.getJSONObject("snow");
            if (api)current.setSnow(joSnow.getDouble("1h"));
        }
        return current;
    }

    private String owmCall(String url) throws IOException {
        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(url)
                .build();

        try (Response response = client.newCall(request).execute()) {
            return response.body().string();
        }
    }
}
