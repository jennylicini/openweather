package eltemps;

import eltemps.domain.Forecast;
import eltemps.domain.Weather;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Orientation;
import javafx.scene.control.ListView;

import java.net.URL;
import java.util.ResourceBundle;

public class Controller2 implements Initializable {

    @FXML
    public ListView<Weather> lvForecast;

    public  ObservableList<Weather> items;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        items = FXCollections.observableArrayList();
    }

    public void setLvForecast(String city) {
        WeatherService ws = new WeatherService();
        Forecast actual = ws.getFiveDay(city);

        ObservableList<Weather> weathers = FXCollections.observableArrayList();
        weathers.addAll(actual.getList());

        try {
            lvForecast.setItems(weathers);
            //Orientacion == HORIZONTAL
            lvForecast.setOrientation(Orientation.HORIZONTAL);
            //lvlcForecast lvlcForecast = new lvlcForecast();
            //lvlcForecast.setListWeather(actual.getList(), actual.getCity());
            lvForecast.setCellFactory(ForecastListView -> new lvlcForecast());
        }catch (Exception e){
            e.printStackTrace();
        }

    }
}
