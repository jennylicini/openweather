package eltemps;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;



public class Controller implements Initializable {
    @FXML
    public ListView<String> lvCiudades;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        cargarCiudades();
        lvCiudades.setCellFactory(ciudadListView -> new lvlcWeather());
    }

    private void cargarCiudades() {
        ObservableList<String> items = FXCollections.observableArrayList (
                "Barcelona", "Madrid", "Valencia", "Sevilla", "Bilbao", "Zaragoza");
        lvCiudades.setItems(items);
    }

    public void ListViewClickElement(MouseEvent mouseEvent) {
        String sCiudad = lvCiudades.getSelectionModel().getSelectedItem().toString() ;
        Stage stage = new Stage();
        Image image = new Image("https://openweathermap.org/themes/openweathermap/assets/img/mobile_app/android_icon.png");
        stage.getIcons().add(image);
        stage.setTitle("Open Weather - Tiempo proximo 5 dias en "+sCiudad);
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("ui/lvForecast.fxml"));
            Parent graf = loader.load();
            Controller2 con2=(Controller2) loader.getController();
            con2.setLvForecast(sCiudad);
            Scene scn = new Scene(graf, 707, 350);
            stage.setResizable(false);
            stage.setScene(scn);
            stage.show();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
}
