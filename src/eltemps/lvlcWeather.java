package eltemps;

import eltemps.domain.Weather;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;

public class lvlcWeather extends ListCell<String> {

    private final WeatherService weatherService = new WeatherService();

    @FXML
    private Label lblciudad;

    @FXML
    private Label lbltemp;

    @FXML
    private Label lbldescription;

    @FXML
    private ImageView imgicon;

    @FXML
    private AnchorPane AnchorPane;

    private FXMLLoader mLLoader;

    @FXML
    private Parent root;

    private short numWin;

    @Override
    protected void updateItem(String city, boolean empty) {
        super.updateItem(city, empty);

        if (empty || city == null) {
            setText(null);
            setGraphic(null);
        } else {
            if (mLLoader == null) {
                mLLoader = new FXMLLoader(getClass().getResource("ui/lcWeather.fxml"));
                mLLoader.setController(this);
                try {
                    mLLoader.load();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            lblciudad.setText(city);
            Weather current = weatherService.getCurrentWeather(city);
            lblciudad.setText(city);
            lbltemp.setText(current.getTemp() + "°C");
            lbldescription.setText(current.getDescription());
            String img = "http://openweathermap.org/img/wn/" + current.getIcon() + "@2x.png";
            showImage(img);
            setGraphic(AnchorPane);
        }
    }

    private void showImage(String img) {
        try {
            Image image = new Image(img);
            imgicon.setImage(image);
            imgicon.setCache(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
